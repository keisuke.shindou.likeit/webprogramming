package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;


public class UserDao {
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続する
      conn = DBManager.getConnection();

      // SELECT文を準備する
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 検索結果は１件のみなのでif文を使用
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {

      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // 全てのユーザーの情報の取得する処理
  public List<User> findAll() {

    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // セレクト文のの準備（findAllでは管理者を除くSQLにすること)）
      String sql = "SELECT * FROM user WHERE is_admin = false";
      // セレクト文の結果を取得し表示する
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);
      // 結果表に格納されたレコード内容を
      // Userインスタンスに追加しArryaListインスタンス追加する
      while (rs.next()) {
        int id = rs.getInt("id");
        String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);
        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベースの切断する
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }



    }
    return userList;
  }

  // 新規登録を行うユーザーの登録を行う。
  public void insert(String loginId, String name, Date birthDate, String password) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続する
      conn = DBManager.getConnection();
      // INSERT文を準備する
      String sql =
          "INSERT INTO user(login_id,name, birth_date, password,create_date, update_date)VALUES(?,?,?,?,now(),now())";

      // INSERT文を実行し、結果表を取得
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setDate(3, birthDate);
      pStmt.setString(4, password);
      // SQLの実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
      return;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return;
        }
      }
    }
  }

  // ユーザー詳細の表示に関する処理
  public User findById(int Id) {
    Connection conn = null;
    try {
      // データベースへ接続する
      conn = DBManager.getConnection();
      // SELECT文を準備する
      String sql = "SELECT * FROM user WHERE id = ?";
      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, Id);
      // SQLの実行
      ResultSet rs = pStmt.executeQuery();
      // 検索結果は１件のみなのでif文を使用
      if (!rs.next()) {
        return null;
      }
      int id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // ユーザーの更新に関する処理
  // 新規登録を行うユーザーの登録を行う。
  public void update(int id, String name, Date birthDate, String password) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続する
      conn = DBManager.getConnection();
      // UPDATE文を準備する
      String sql =
          "UPDATE user SET name = ?,birth_date = ?,password = ?,update_date = now() WHERE id = ?";

      // UPDATE文を実行し、結果表を取得
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setDate(2, birthDate);
      pStmt.setString(3, password);
      pStmt.setInt(4, id);
      // SQLの実行
      pStmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      return;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return;
        }

}
    }
  }

  // ユーザーの削除に関する処理
  public void delete(int id) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続する
      conn = DBManager.getConnection();
      // DELETE文を準備する
      String sql = "DELETE FROM user WHERE id = ?";
      // SELECTを実行し、結果表を取得
      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      // SQLの実行
      pStmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      return;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return;
        }

}
    }
  }

  // ユーザーの検索に関する処理
  public List<User> serch(String loginId, String name, Date startDate, Date endDate) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();
    PreparedStatement pStmt = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // セレクト文のの準備
      String sql =
          "SELECT * FROM user WHERE is_admin = false";
      // StringBuilderの準備
      StringBuilder stb = new StringBuilder(sql);
      // 条件分岐
      // ログインIDが入力された場合
      if (!loginId.equals("")) {
        stb.append(" AND login_id = ? ");// sql += " AND login_id =?";
      }
      // 名前が入力された場合
      if (!name.equals("")) {
        stb.append(" AND name LIKE ? "); // sql += "AND name = ?";
      }
      // 誕生日（開始日）が入力された場合
      if (startDate != null) {
        stb.append(" AND birth_date >=  ?"); // sql += "AND birth_date BETWEEN ?";
      }
      if (endDate != null) {
        stb.append(" AND birth_date <= ? "); // sql += "AND ?";
      }
      // セレクト文の結果を取得し表示する
      pStmt = conn.prepareStatement(stb.toString());
      // ログインIDのみ入力された場合
      if (!loginId.equals("")) {
        pStmt.setString(1, loginId);
      }
      // 名前のみ入力された場合
      if (!name.equals("")) {
        pStmt.setString(1, "%" + name + "%");
      }
   // 名前とログインIDが入力された場合
      if (!loginId.equals("") && !name.equals("")) {
        pStmt.setString(1, loginId);
        pStmt.setString(2, "%" + name + "%");
      }
      // 誕生日のみ入力された場合
      if (startDate != null && endDate != null) {
        pStmt.setDate(1, startDate);
        pStmt.setDate(2, endDate);
      }
      // 名前と誕生日が入力された場合
      if (!name.equals("") && startDate != null && endDate != null) {
        pStmt.setString(1, "%" + name + "%");
        pStmt.setDate(2, startDate);
        pStmt.setDate(3, endDate);
      }
      // ログインIDと誕生日がされた場合
      if (!loginId.equals("") && startDate != null && endDate != null) {
        pStmt.setString(1, loginId);
        pStmt.setDate(2, startDate);
        pStmt.setDate(3, endDate);
      }
     
      // 全てが入力された場合
      if (!loginId.equals("") && !name.equals("") && startDate != null && endDate != null) {
        pStmt.setString(1, loginId);
        pStmt.setString(2, "%" + name + "%");
        pStmt.setDate(3, startDate);
        pStmt.setDate(4, endDate);
      }

      // SQL文を実行する。
      ResultSet rs = pStmt.executeQuery();
      // 結果表に格納されたレコード内容を
      // Userインスタンスに追加しArryaListインスタンス追加する
      while (rs.next()) {
        int id = rs.getInt("id");
        String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
        String _name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, _loginId, _name, birthDate, _password, isAdmin, createDate, updateDate);
        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベースの切断する
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;

  }

  // ログインIDが既に存在するのかの処理
  public boolean findByLoginId(String loginId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続する
      conn = DBManager.getConnection();
      // DELETE文を準備する
      String sql = " SELECT * FROM user WHERE login_id = ?";
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      // SQLの実行
      ResultSet rs =   pStmt.executeQuery();
      while (rs.next()) {
        return true;
      }
      return false;

      } catch (SQLException e) {
      e.printStackTrace();
      return false;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    }
}

// パスワードだけ更新されなかった場合の処理
public void updateExcludePassword(int id, String name, Date birthdate) {
  Connection conn = null;
  PreparedStatement stmt = null;
  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE id = ?";

    stmt = conn.prepareStatement(sql);
    stmt.setString(1, name);
    stmt.setDate(2, birthdate);
    stmt.setInt(3, id);

    // SQL実行
    stmt.executeUpdate();

  } catch (SQLException e) {
    e.printStackTrace();
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}

}

