package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインチェック
      // "userInfo"の情報を取得
      // セッションスコープを準備する
      // ログインに失敗した場合"LoginServlet"へリダイレクトする。最後はreturnで返す。
      // ログインチェック
      HttpSession session = request.getSession();
      User logCheck = (User) session.getAttribute("userInfo");
      if (logCheck == null) {
        // ログイン画面へリダイレクト
        response.sendRedirect("LoginServlet");
        return;
      }
      // idをgetParameterメソッドで取得
      int id = Integer.valueOf(request.getParameter("id"));
      // UserDaoクラスのfindIdメソッドを呼び出す
      UserDao userDao = new UserDao();
      // IDに紐づく情報を取得
      User user = userDao.findById(id);
      request.setAttribute("user", user);
      // フォワード文でussDetail.jspに遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
      dispatcher.forward(request, response);
      return;
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
