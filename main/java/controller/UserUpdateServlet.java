package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // ログインチェック
      // "userInfo"の情報を取得
      // ログインに失敗した場合"LoginServlet"へリダイレクトする。最後はreturnで返す。
      // ログインチェック
      HttpSession session = request.getSession();
      User logCheck = (User) session.getAttribute("userInfo");
      if (logCheck == null) {
        // ログイン画面へリダイレクト
        response.sendRedirect("LoginServlet");
        return;
      }
      // idをgetParameterメソッドで取得
      int id = Integer.valueOf(request.getParameter("id"));
      // UserDaoクラスのfindIdメソッドを呼び出す
      UserDao userDao = new UserDao();
      User user = userDao.findById(id);
      request.setAttribute("user", user);
      // フォワード文でussDetail.jspに遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // submit(更新ボタン)が押された際にdoPostが起動
      // 文字化け防止
      request.setCharacterEncoding("UTF-8");
      // フォームの値を受け取る。パスワードと確認用のパスワードと誕生日とユーザー名を取得
      // jspのformで指定したname="○○"と合わせる
      int id = Integer.valueOf(request.getParameter("user-id"));
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      String password = request.getParameter("password");
      String passwordCon = request.getParameter("password-confirm");
      // UserDaoのuodateメソッドを呼び出す
      UserDao userDao = new UserDao();
      // String型の誕生日をDate型に戻す。
      Date _birthDate = Date.valueOf(birthDate);
      // 名前空欄またはパスワードが一致していない場合の処理
      if (name.equals("") || !password.equals(passwordCon)) {
        // リクエストスコープにエラーメッセージをセット
        User user = userDao.findById(id);
        user.setName(name);
        user.setBirthDate(Date.valueOf(birthDate));
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        request.setAttribute("user", user);
        // ログインjspにフォワード("errMsg"と"loginIdとnameとbirthDateを保存")
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }
      // 誕生日が未入力の場合
      if (birthDate.equals("")) {
        User user = userDao.findById(id);
        user.setName(name);
        user.setBirthDate(null);
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        request.setAttribute("user", user);
        // ログインjspにフォワード("errMsg"と"loginIdとnameとbirthDateを保存")
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }
      // パスワードの更新がなかった場合
      if (password.equals("") && passwordCon.equals("")) {
        userDao.updateExcludePassword(id, name, _birthDate);
        // パスワードの更新があった場合
      } else {
        // パスワードの暗号化
        String encordedPassword = PasswordEncorder.encordPassword(password);
        userDao.update(id, name, _birthDate, encordedPassword);
      }
      // UserListServletへリダイレクト
      response.sendRedirect("UserListServlet");
    }

  }



