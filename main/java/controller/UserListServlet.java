package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserListServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインチェック
    // "userInfo"の情報を取得
    // ログインに失敗した場合"LoginServlet"へリダイレクトする。最後はreturnで返す。
    // ログインチェック
    HttpSession session = request.getSession();
    Object logCheck = session.getAttribute("userInfo");
    if (logCheck == null) {
      // ログイン画面へリダイレクト
      response.sendRedirect("LoginServlet");
      return;
    }
    // UserDaoクラスのfindAllメソッドを呼び出す。
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();
    // UserDaoクラスのfindAllメソッドの戻り値(userList)をセットする
    request.setAttribute("userList", userList);
    // userList.japにフォワードをする
    // ユーザー一覧画面を表示
    RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
    dispatchar.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // submit(検索ボタン)が押された際にdoPostが起動
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");
    // フォームの値を受け取る。ログインIDと名前と誕生日を取得
    // jspのformで指定したname="○○"と合わせる
    String loginId = request.getParameter("user-loginid");
    String userName = request.getParameter("user-name");
    String startDate = request.getParameter("date-start");
    String endDate = request.getParameter("date-end");
    // UserDaoのserchメソッドを呼び出す。
    UserDao userDao = new UserDao();
    // リクエストスコープにログインIDと名前と誕生日をセット
    request.setAttribute("loginId", loginId);
    request.setAttribute("userName", userName);
    request.setAttribute("startDate", startDate);
    request.setAttribute("endDate", endDate);
    // 誕生日をデータ型に変換
    Date _startDate = null;
    if (!startDate.equals("")) {
      _startDate = Date.valueOf(startDate);
    }
    Date _endDate = null;
    if (!endDate.equals("")) {
      _endDate = Date.valueOf(endDate);
    }
    List<User> userSerch = userDao.serch(loginId, userName, _startDate, _endDate);
    // UserDaoクラスのserchメソッドの戻り値(user)をセットする
    request.setAttribute("userList", userSerch);
    // userList.japにフォワードをする
    // ユーザー一覧画面を表示
    RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
    dispatchar.forward(request, response);

  }

}
