package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;


  /**
   * @see HttpServlet#HttpServlet()
   */
  public LoginServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    /**** ログインの有無確認 start ****/
    // ログイン情報を取得
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");
    // ログイン情報があればユーザー一覧へ遷移
    if (user != null) {
      response.sendRedirect("UserListServlet");
      return;
    }
    // ログイン画面の表示userLogin.jspに遷移
    RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
    dispatchar.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // submit(ログインボタン)が押された際にdoPostが起動
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    // ログインIDとパスワードを取得
    // jspのformで指定したname="○○"と合わせる
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    // UserDaoクラスのfindByLoginメソッドを呼び出す
    UserDao userDao = new UserDao();
    // 入力されたパスワードを暗号化
    String encorderdPassword = PasswordEncorder.encordPassword(password);
    User user = userDao.findByLoginInfo(loginId, encorderdPassword);
    // ログイン失敗時（DBにユーザー情報がなかった場合）
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ログインＩＤまたはパスワードが異なります");
      // 入力したログインIDを画面上に表示
      request.setAttribute("user-loginid", loginId);
      // ログインjspにフォワード("errMsg"と"loginIdを保存")
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;

    } // ログイン成功時（あった場合）
    HttpSession session = request.getSession();
    // セッションスコープにユーザー情報をセット
    session.setAttribute("userInfo", user);
    // ユーザー一覧画面へリダイレクト
    response.sendRedirect("UserListServlet");


  }

}
