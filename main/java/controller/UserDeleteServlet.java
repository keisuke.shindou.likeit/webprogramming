package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインチェック
      // "userInfo"の情報を取得
      // ログインに失敗した場合"LoginServlet"へリダイレクトする。最後はreturnで返す。
      // ログインチェック
      HttpSession session = request.getSession();
      User logCheck = (User) session.getAttribute("userInfo");
      if (logCheck == null) {
        // ログイン画面へリダイレクト
        response.sendRedirect("LoginServlet");
        return;
      }
      // idをgetParameterメソッドで取得
      int id = Integer.valueOf(request.getParameter("id"));
      // UserDaoクラスのfindIdメソッドを呼び出す
      UserDao userDao = new UserDao();
      User user = userDao.findById(id);
      request.setAttribute("user", user);
      // フォワード文でussDetail.jspに遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
      return;
    }



    /**
     * 
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // submit(更新ボタン)が押された際にdoPostが起動
      // 文字化け防止
      request.setCharacterEncoding("UTF-8");
      // フォームの値(id)を受け取る。
      // jspのformで指定したname="○○"と合わせる
      int id = Integer.valueOf(request.getParameter("user-id"));
      // ユーザー一覧画面へリダイレクト
      UserDao dao = new UserDao();
      dao.delete(id);
      response.sendRedirect("UserListServlet");
      return;
	}

}
