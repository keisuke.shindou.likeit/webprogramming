package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;
/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    /**** ログインの有無確認 start ****/
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // ログイン情報がない場合
    if (user == null) {
      // セッションがなかったらログイン画面へ
      response.sendRedirect("LoginServlet");
      return;
    }
    // ログイン成功時（あった場合）
    // ログイン成功時はuserAdd.jspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */



  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // submit(登録ボタン)が押された際にdoPostが起動
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");
    // フォームの値を受け取る。ログインIDとパスワードと確認用のパスワードと誕生日とユーザー名を取得
    // jspのformで指定したname="○○"と合わせる
    String loginId = request.getParameter("user-loginid");
    String name = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");
    String password = request.getParameter("password");
    String passwordCon = request.getParameter("password-confirm");
    UserDao userDao = new UserDao();
    boolean existsLoginId = userDao.findByLoginId(loginId);
    // 名前と誕生日とパスワードが未入力、パスワードが一致していない場合の処
    if (loginId.equals("") || name.equals("") || birthDate.equals("") || password.equals("")
        || !password.equals(passwordCon) || existsLoginId) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力された内容は正しくありません");
      // 入力したログインIDと名前と誕生日を画面上に表示
      request.setAttribute("inputloginId", loginId);
      request.setAttribute("inputname", name);
      request.setAttribute("inputbirthDate", birthDate);
      // ログインjspにフォワード("errMsg"と"loginIdとnameとbirthDateを保存")
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    } // 新規登録成功時
    Date _birthDate = Date.valueOf(birthDate);
    // パスワードを暗号化
    String encordedPassword = PasswordEncorder.encordPassword(password);
    // もし問題がなく登録出来る場合
    // データ型に型変換を行う。
    // ユーザー一覧画面へリダイレクト
    userDao.insert(loginId, name, _birthDate, encordedPassword);
    response.sendRedirect("UserListServlet");

  }

}







